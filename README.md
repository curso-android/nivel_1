# Curso de programación de aplicaciones Android con KOTLIN

Bienvenido a este curso de aplicaciones Android con Kotlin. Espero puedas lograr los proyectos que tengas en mente
comenzando desde lo más simple del desarrollo de aplicaciones. En este curso trataré de compartirte mi conocimiento
y experiencia en el desarrollo Android.

Mi nombre es Ernesto Gálvez Martínez, soy estudiante de octavo semestre de la Facultad de Ingeniería.
Actualmente me dedico a desarrollar alicaciones de Android y Android Things para SOFTEL, una empresa de
desarrollo de tecnología. Fui parte del laboratorio de ```UNAM Mobile``` también haciendo desarrollo móvil.
He formado parte de proyectos en ```MetLife``` y desarrollado sistemas de automatización en las estéticas de
Silvia Galván. Tengo ya 2 años de experiencia en el desarrollo de aplicaciones móviles y espero
poder cubrir tus expectativas a lo largo de este curso.

Mi correo electrónico para aclaración de cualquier duda dentro del mundo de Android es:
```egm.droid@gmail.com```

Este repositorio estará disponible a lo largo del curso para consulta fuera del curso (como un ejemplo de lo
visto durante las clases).

## Instalar herramientas de Android Studio

1. Acceder a Android Studio.
2. En la pantalla que muestra de inicio, seleccionar el menú dropdown de ```Configure```.

![Acceso a descarga externo](img/as_configure.png)

3. Seleccionar la Opción de Settings.
    - Si ya estabas dentro de Android Studio en un proyecto, el acceso se hace en el menú ```File -> Settings```.
    - El resto de pasos es el mismo para ambos casos.
4. Seguir el siguiente camino hasta las herramientas: ```Appeareance & Behavior -> System Settings -> Android SDK```.
    - En la pestaña de SDK Platforms, tenemos los sistemas operativos de Android. Dichos sistemas operativos servirán
    posteriormente para crear dispositivos virtuales, y además tener en Android Studio las herramientas para desarrollar
    para esos sistemas.
    - Para motivos del curso se recomienda que se descarguen la versión ```Oreo (API 27)``` y ```Pie (API 28)```.
    - Siempre es recomendable tener la versión más actual de android para mantener el IDE con las últimas herramientas,
    el resto se pueden utilizar para dar soporte a versiones anteriores (por ejemplo, API 19) y tener una manera de
    comprobar cómo se verá nuestra apĺicación en versiones más antiguas de Android (desde el emulador)
    
![SDK PLATFORMS](img/android_sdk.png)

5. En la pestaña de tools se descargan las herramientas de desarrollo para Android Studio, como los servicios de Google,
las herramientas de plataforma (asociadas al sistema operativo android), emuladores, etc.

![SDK TOOLS](img/sdk_tools.png)

6. Es necesario colocar en el ```Android SDK Location``` la dirección de la carpeta SDK dentro de tu ordenador.
Esto es importante ya que ahí es donde se encuentran todas las herramientas de Android Studio, y si te encuentras en
sistema operativo Windows, por default se encuentra en las carpetas de sistema.


## Opcional
Cuando se trabaja con imágenes en Android, existen múltiples maneras de importarlas a un proyecto. Android maneja
una misma imagen en diferentes densidades (para hacer más responsiva la aplicación), por lo que es recomendable
instalar un plugin que nos ayude con la importación de +iconos, imágenes, entre otros recursos similares en todas
sus dimensiones disponibles.

1. Accedemos al mismo menú ```Settings``` previamente accesado.
2. En la sección de ```Plugin``` nos vamos al buscador (parte superior del diálogo).
3. Buscamos la palabra ```drawable```, y la búsqueda arroja un plugin llamado ```Android Drawable Importer```.
4. Instalamos el plugin.

![Android Drawable Importer](img/drawable_plugin.png)

## ESTAMOS LISTOS PARA COMENZAR EL CURSO!!